/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */
package astrolabe.kernelcache.actions;

import astrolabe.kernelcache.lib.KernelCache;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingWorker;
import org.netbeans.api.progress.ProgressHandle;
import org.netbeans.api.progress.ProgressHandleFactory;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

@ActionID(
        category = "Tools",
        id = "astrolabe.kernelcache.actions.UpdateDefaultKernels"
)
@ActionRegistration(
        iconBase = "astrolabe/kernelcache/actions/download_24dp_0C234B_FILL0_wght400_GRAD0_opsz24_16x16.png",
        displayName = "#CTL_UpdateDefaultKernels"
)
@ActionReference(path = "Menu/Tools", position = 2000)
@Messages("CTL_UpdateDefaultKernels=Update default NAIF SPICE kernels")
public final class UpdateDefaultKernels implements ActionListener {

    private static final Logger LOG = Logger.getLogger(UpdateDefaultKernels.class.getName());

    private static final String[] defaultKernels = {
        "lsk/naif0012.tls",
        "pck/pck00011.tpc",
        "spk/planets/de432s.bsp",};

    @Override
    public void actionPerformed(ActionEvent e) {
        LOG.log(Level.INFO, "Starting update.");
        new Worker().execute();
    }

    private class Worker extends SwingWorker<Void, Integer> {

        private final ProgressHandle ph;

        public Worker() {
            ph = ProgressHandleFactory.createUIHandle("Updating NAIF kernels...", () -> this.cancel(true), null);
            ph.start(defaultKernels.length + 1);
        }

        @Override
        protected Void doInBackground() throws Exception {
            int i = 0;
            for (var kernel : defaultKernels) {
                try {
                    publish(i++);
                    var location = KernelCache.get("NAIF", kernel);
                    LOG.log(Level.INFO, location.toString());
                } catch (Exception ex) {
                    Exceptions.printStackTrace(ex);
                }
            }

            return null;
        }

        @Override
        protected void process(List<Integer> li) {
            for (var i : li) {
                ph.progress(i);
            }
        }

        @Override
        protected void done() {
            ph.finish();
        }
    }
}
