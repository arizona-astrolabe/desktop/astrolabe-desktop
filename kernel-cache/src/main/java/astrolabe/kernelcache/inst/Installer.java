/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University
 * of Arizona. All rights reserved.
 *
 * This file is part of Astrolabe. Astrolabe is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as  published by the Free Software Foundation, either version 3 of
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with Astrolabe.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package astrolabe.kernelcache.inst;

import astrolabe.kernelcache.actions.UpdateDefaultKernels;
import astrolabe.metadata.inst.BuildInfo;
import astrolabe.metadata.store.Store;
import edu.arizona.space.jnaspice.JnaSpiceLibraryFactory;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingUtilities;
import org.openide.modules.ModuleInstall;
import org.openide.util.Utilities;
import org.openide.windows.WindowManager;

public class Installer extends ModuleInstall {

    private static final Logger LOG = Logger.getLogger(Installer.class.getName());

    @Override
    public void restored() {
        LOG.log(Level.INFO, "kernel-cache plugin loaded");
        WindowManager.getDefault().invokeWhenUIReady(() -> {
            // Put our build information in the store and then make sure the SPICE library is loaded.
            try {
                // Put build info in the store.
                String buildInfo = BuildInfo.get();
                LOG.log(Level.INFO, "{0} buildInfo.kernel-cache={1}", new Object[]{new Throwable().getStackTrace()[0].toString(), buildInfo});
                Store.put("buildInfo.kernel-cache", buildInfo);

                // Put the jnaspice2 version information in the data store.
                var jsl = JnaSpiceLibraryFactory.getInstance();
                String ver = jsl.tkvrsn_c("TOOLKIT");
                Store.put("cspice.version", ver);
            } catch (IOException ex) {
                LOG.log(Level.INFO, ex.getMessage());
                Arrays.asList(ex.getStackTrace()).stream().forEach((ste) -> LOG.log(Level.INFO, ste.toString()));
            }

            // After that, do the action that makes sure the default kernels are downloaded.
            var udk = Utilities.actionsGlobalContext().lookup(UpdateDefaultKernels.class);
            if (udk != null) {
                SwingUtilities.invokeLater(() -> udk.actionPerformed(new ActionEvent(this, ActionEvent.ACTION_PERFORMED, "")));
            }
        });
    }
}
