/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University
 * of Arizona. All rights reserved.
 *
 * This file is part of Astrolabe. Astrolabe is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as  published by the Free Software Foundation, either version 3 of
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with Astrolabe.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package astrolabe.kernelcache.options;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import javax.swing.JComponent;
import javax.swing.SwingUtilities;
import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.HelpCtx;
import org.openide.util.Lookup;

@OptionsPanelController.SubRegistration(
        location = "Astrolabe",
        displayName = "#AdvancedOption_DisplayName_KernelCache",
        keywords = "#AdvancedOption_Keywords_KernelCache",
        keywordsCategory = "Astrolabe/KernelCache"
)
@org.openide.util.NbBundle.Messages({"AdvancedOption_DisplayName_KernelCache=Kernel Cache", "AdvancedOption_Keywords_KernelCache=SPICE kernel cache"})
public final class KernelCacheOptionsPanelController extends OptionsPanelController {

    private KernelCachePanel panel;
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private boolean changed;

    @Override
    public void update() {
        getPanel().load();
        changed = false;
    }

    @Override
    public void applyChanges() {
        SwingUtilities.invokeLater(() -> {
            getPanel().store();
            changed = false;
        });
    }

    @Override
    public void cancel() {
        // need not do anything special, if no changes have been persisted yet
    }

    @Override
    public boolean isValid() {
        return getPanel().valid();
    }

    @Override
    public boolean isChanged() {
        return changed;
    }

    @Override
    public HelpCtx getHelpCtx() {
        return null; // new HelpCtx("...ID") if you have a help set
    }

    @Override
    public JComponent getComponent(Lookup masterLookup) {
        return getPanel();
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener l) {
        pcs.addPropertyChangeListener(l);
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener l) {
        pcs.removePropertyChangeListener(l);
    }

    private KernelCachePanel getPanel() {
        if (panel == null) {
            panel = new KernelCachePanel(this);
        }
        return panel;
    }
}
