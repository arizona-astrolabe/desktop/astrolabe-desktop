/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University
 * of Arizona. All rights reserved.
 *
 * This file is part of Astrolabe. Astrolabe is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as  published by the Free Software Foundation, either version 3 of
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with Astrolabe.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package astrolabe.kernelcache;

import astrolabe.kernelcache.lib.KernelCache;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 *
 * @author asg
 */
public class KernelTest {

    private static final Logger LOG = Logger.getLogger(KernelTest.class.getName());

    private static final Path db = Path.of(System.getProperty("user.home") + "/" + ".astrolabe-data/kernel-cache.h2.mv.db");
    private static final Path dbBak = Path.of(System.getProperty("user.home") + "/" + ".astrolabe-data/kernel-cache.h2.mv.db.bak");
    private static final Path dbDir = Path.of(System.getProperty("user.home") + "/" + ".astrolabe-data/kernel-cache/");
    private static final Path dbDirBak = Path.of(System.getProperty("user.home") + "/" + ".astrolabe-data/kernel-cache-bak/");

    public KernelTest() {
    }

    @BeforeAll
    public static void setUpClass() throws IOException {
        // See if we need to move the database out of the way so we don't stomp it.
        if (db.toFile().exists()) {
            LOG.log(Level.INFO, "{0} -> {1}", new String[]{db.toFile().getCanonicalPath(), dbBak.toFile().getCanonicalPath()});
            Files.move(db, dbBak, StandardCopyOption.REPLACE_EXISTING);
        }
        if (dbDir.toFile().exists()) {
            LOG.log(Level.INFO, "{0} -> {1}", new String[]{dbDir.toFile().getCanonicalPath(), dbDirBak.toFile().getCanonicalPath()});
            if (dbDirBak.toFile().exists()) {
                Files.walk(dbDirBak).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
            }
            Files.move(dbDir, dbDirBak, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    @AfterAll
    public static void tearDownClass() throws IOException {
        // See if we need to move things back.
        if (dbBak.toFile().exists()) {
            LOG.log(Level.INFO, "{0} -> {1}", new String[]{dbBak.toFile().getCanonicalPath(), db.toFile().getCanonicalPath()});
            Files.move(dbBak, db, StandardCopyOption.REPLACE_EXISTING);
        }
        if (dbDirBak.toFile().exists()) {
            LOG.log(Level.INFO, "{0} -> {1}", new String[]{dbDirBak.toFile().getCanonicalPath(), dbDir.toFile().getCanonicalPath()});
            Files.walk(dbDir).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
            Files.move(dbDirBak, dbDir, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void test1() throws IllegalArgumentException, IOException, URISyntaxException {
        assertEquals(false, KernelCache.contains("NAIF", "lsk/naif0012.tls"));

        var path = KernelCache.get("NAIF", "lsk/naif0012.tls");
        assertNotNull(path);
        System.out.println(path);

        var loc = KernelCache.contains("NAIF", "lsk/naif0012.tls");
        assertTrue(loc);
        System.out.println(loc);
    }

    @Test
    public void test2() throws IllegalArgumentException, IOException, URISyntaxException {
        assertEquals(false, KernelCache.contains("NAIF", "pck/pck00011.tpc"));

        var path = KernelCache.get("NAIF", "pck/pck00011.tpc");
        assertNotNull(path);
        System.out.println(path);

        var loc = KernelCache.contains("NAIF", "pck/pck00011.tpc");
        assertTrue(loc);
        System.out.println(loc);
    }

}
