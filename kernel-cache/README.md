# kernel-cache

This module contains code that implements a cache of NAIF SPICE kernels.
The cache can have many inputs, but by default it contains only one, the NAIF
Generic Kernels set. The index and the kernels are stored in `~/.kernel-cache`
and references to kernels retrieved from the index will have that file name
prefix.

