# Astrolabe Desktop 23

![Astrolabe Logo](assets/astrolabe-logo-256.png)

Astrolabe Desktop 23 is  a desktop application built on the 
[Netbeans Platform v23](https://netbeans.apache.org/kb/docs/platform/) using 
[Java Development Kit 21](https://openjdk.org/projects/jdk/21/). 
Astrolabe is built at the University of Arizona's Space Institute for 
spacecraft mission planning and operations. 
Astrolabe development started during the 
[OSIRIS-REx mission](https://www.asteroidmission.org/) as 
[JPlanner](https://ieeexplore.ieee.org/document/9843365), 
a tool to automate the operations engineers' workflow.

## Developer's Notes.

### Introduction.

Astrolabe Desktop is developed in two parts. The code here is the public 
portion that is common to every build of Astrolabe. This repository includes:
- `application`: The Maven project that collects all of the Netbeans modules
  into a module suite. Note that the Maven profile you use here is important
  because different modules will be pulled from your Maven cache based on the
  profile you choose. Profiles are generally mission-specific.
- `assets`: Image assets used by multiple modules.
- `branding`: The Netbeans module that defines the branding (logos, etc.) for
  the Netbeans module suite.
- `kernel-cache`: One of the default included modules. Implements a cache
  of NAIF SPICE kernels and includes a JNA-accessible version of CSPICE
  for other modules to use.
- `metadata`: A simple key-value store to ease integration.
- `netbeans-nbpackage`: A Git submodule reference to the Apache project
  that generates installers for Netbeans module suite applications.
- `public`: The assets that GitLab will use to create the update centers
  for the modules included in the default set.

### Getting started.

#### `~/.m2/settings.xml`

Before you clone the repository, you will need to make a couple of additions 
to your Maven configuration in order to be able to produce an official build.

First, code signing. The Maven configuration files in this project assume that
you have a code signing key that you've configured Maven accordingly. If you 
want to build and not sign the modules you build, you can change the `pom.xml` 
files accordingly. If you do want to sign modules, you'll need to add this to
`~/.m2/settings.xml`, and you will need to add a similar clause for each 
profile that you build with. `netbeans-ide` is the default. The others are 
project/program specific.

```xml
    <profiles>
        <profile>
            <id>netbeans-ide</id>
            <properties>
                <keystore.location>PATH_TO_KEYSTORE</keystore.location>
                <keystore.alias>KEY_NAME_IN_STORE</keystore.alias>
                <keystore.password>KEYSTORE_PASSWORD</keystore.password>
            </properties>
        </profile>
        ...
    </profiles>
```

I also suggest adding the following two lines to each profile, but they aren't
code-signing specific:

```xml
<downloadSources>true</downloadSources>
<downloadJavadocs>true</downloadJavadocs>
```

Second, Maven repositories. If you intend to push to the Maven
repository associated with this GitLab project, you will need to add an 
access token. Getting a GitLab access token is beyond the scope of this document.
When you have it, you'll need to add a clause to the `<servers>` section of
your Maven settings.

```xml
        <server>
            <id>astrolabe-maven-public</id>
            <username>YOUR_USERNAME</username>
            <password>YOUR_ACCESS_TOKEN</password>
            <configuration>
                <authenticationInfo>
                    <userName>YOUR_USERNAME</userName>
                    <password>YOUR_ACCESS_TOKEN</password>
                </authenticationInfo>
            </configuration>
        </server>
```

### Clone.

Next, clone and initialize submodules:
```
git clone --recurse-submodules git@gitlab.com:arizona-astrolabe/desktop/astrolabe-desktop.git
```

You can then open this Maven project in Netbeans, or `cd astrolabe-desktop` and `mvn install` to build
the entire suite. You can run the application by running the `astrolabe-app` 
subproject inside `astrolabe-desktop`, or you can `cd astrolabe-desktop/application` and do
`mvn -Pnetbeans-ide nbm:cluser-app nbm:run-platform`. You can switch `netbeans-ide` for the
appropriate profile name.

---

Copyright (C) 2016-2024 Arizona Board of Regents on behalf of the University 
of Arizona.

![GNU GPL v3 logo](https://www.gnu.org/graphics/gplv3-127x51.png)

This program is free software: you can redistribute it and/or modify it under 
the terms of the GNU General Public License as published by the Free Software 
Foundation, either version 3 of the License, or (at your option) any later 
version. This program is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more 
details. You should have received a copy of the GNU General Public License 
along with this program. If not, see <https://www.gnu.org/licenses/>.
