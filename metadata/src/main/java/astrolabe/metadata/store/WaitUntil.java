/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 */
package astrolabe.metadata.store;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.SwingWorker;
import org.openide.util.Exceptions;

/**
 * Wait for a value to be set in the metadata store and then run a method.
 *
 * @author asg
 */
public class WaitUntil {

    private static class Waiter extends SwingWorker<Void, Void> {

        private final String key;
        private final ActionListener al;

        public Waiter(String key, ActionListener al) {
            this.key = key;
            this.al = al;
        }

        @Override
        protected Void doInBackground() throws Exception {
            try {
                String val;
                while (!Store.containsKey(key)) {
                    try {
                        Thread.sleep(250);
                    } catch (InterruptedException ie) {
                        Exceptions.printStackTrace(ie);
                    }
                }
                val = Store.get(key);

                // Do the associated action.
                ActionEvent ae = new ActionEvent(this, 0, val);
                al.actionPerformed(ae);
            } catch (Exception e) {
                Exceptions.printStackTrace(e);
                throw e;
            }
            return null;
        }
    }

    /**
     * Wait for a value to be set and then run a method.
     *
     * @param key What key to wait to be set.
     * @param al What to do when it is.
     */
    public static void isSet(String key, ActionListener al) {
        Waiter w = new Waiter(key, al);
        w.execute();
    }
}
