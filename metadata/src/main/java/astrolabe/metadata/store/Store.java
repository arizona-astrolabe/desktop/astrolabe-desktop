/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 */
package astrolabe.metadata.store;

import java.util.LinkedList;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * A simple key-value store that's accessible throughout an application.
 *
 * @author asg
 */
public class Store {

    private static final ConcurrentHashMap<String, String> DATA_STORE_MAP;
    private static final LinkedList<StoreListener> LISTENERS;

    static {
        DATA_STORE_MAP = new ConcurrentHashMap<>();
        LISTENERS = new LinkedList<>();
    }

    public static void addListener(StoreListener sl) {
        synchronized (LISTENERS) {
            LISTENERS.add(sl);
        }
    }

    public static void removeListener(StoreListener sl) {
        synchronized (LISTENERS) {
            LISTENERS.remove(sl);
        }
    }

    public static String get(String key) {
        return DATA_STORE_MAP.get(key);
    }

    public static String get(String key, String def) {
        return DATA_STORE_MAP.getOrDefault(key, def);
    }

    public static void put(String key, String newVal) {
        String oldval = DATA_STORE_MAP.get(key);
        DATA_STORE_MAP.put(key, newVal);
        for (StoreListener sl : LISTENERS) {
            sl.dataModified(key, oldval, newVal);
        }
    }

    public static boolean containsKey(String key) {
        return DATA_STORE_MAP.containsKey(key);
    }

    public static Set<String> getKeys() {
        return DATA_STORE_MAP.keySet();
    }
}
