/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 */
package astrolabe.metadata.inst;

import java.io.IOException;
import java.util.List;
import java.util.jar.Manifest;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BuildInfo {

    private static final Logger LOG = Logger.getLogger(BuildInfo.class.getName());
    private static final List<String> keys = List.of("OpenIDE-Module-Implementation-Version", "OpenIDE-Module-Build-Version");

    public static String get() throws IOException {
        StringBuilder sb = new StringBuilder();

        var manifest = new Manifest(BuildInfo.class.getClassLoader().getResourceAsStream("META-INF/MANIFEST.MF"));
        for (var attr : manifest.getMainAttributes().entrySet()) {
            LOG.log(Level.INFO, "{0} = {1}", new Object[]{attr.getKey(), attr.getValue()});
            for (var key : keys) {
                if (attr.getKey().toString().contains(key)) {
                    sb.append(attr.getValue().toString());
                    if (!sb.toString().contains("/")) {
                        sb.append("/");
                    }
                }
            }
        }

        return sb.toString();
    }
}
