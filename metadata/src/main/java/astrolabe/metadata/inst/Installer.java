/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University
 * of Arizona. All rights reserved.
 *
 * This file is part of Astrolabe. Astrolabe is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as  published by the Free Software Foundation, either version 3 of
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with Astrolabe.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package astrolabe.metadata.inst;

import astrolabe.metadata.store.Store;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.modules.ModuleInstall;
import org.openide.windows.WindowManager;

public class Installer extends ModuleInstall {

    private static final Logger LOG = Logger.getLogger(Installer.class.getName());

    @Override
    public void restored() {
        LOG.log(Level.INFO, "metadata plugin loaded");
        WindowManager.getDefault().invokeWhenUIReady(() -> {
            try {
                // Put build info in the store.
                String buildInfo = BuildInfo.get();
                LOG.log(Level.INFO, "{0} buildInfo.metadata={1}", new Object[]{new Throwable().getStackTrace()[0].toString(), buildInfo});
                Store.put("buildInfo.metadata", buildInfo);
            } catch (IOException ex) {
                LOG.log(Level.INFO, ex.getMessage());
                Arrays.asList(ex.getStackTrace()).stream().forEach((ste) -> LOG.log(Level.INFO, ste.toString()));
            }
        });
    }
}
