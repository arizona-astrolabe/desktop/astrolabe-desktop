/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University
 * of Arizona. All rights reserved.
 *
 * This file is part of Astrolabe. Astrolabe is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as  published by the Free Software Foundation, either version 3 of
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with Astrolabe.
 * If not, see <https://www.gnu.org/licenses/>.
 */
@OptionsPanelController.ContainerRegistration(id = "Astrolabe", categoryName = "#OptionsCategory_Name_Astrolabe", iconBase = "astrolabe/options/astrolabe-logo-32.png", keywords = "#OptionsCategory_Keywords_Astrolabe", keywordsCategory = "Astrolabe")
@NbBundle.Messages(value = {"OptionsCategory_Name_Astrolabe=Astrolabe", "OptionsCategory_Keywords_Astrolabe=Astrolabe"})
package astrolabe.options;

import org.netbeans.spi.options.OptionsPanelController;
import org.openide.util.NbBundle;
