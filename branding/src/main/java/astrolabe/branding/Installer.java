/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 */
package astrolabe.branding;

import astrolabe.common.meta.BuildInfo;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.modules.ModuleInstall;
import org.openide.windows.WindowManager;

/**
 * Installer class to load build properties.
 *
 * @author agardner
 */
public class Installer extends ModuleInstall {

    private static final Logger LOG = Logger.getLogger(Installer.class.getName());

    /**
     * Default constructor.
     */
    public Installer() {
    }

    @Override
    public void restored() {
        WindowManager.getDefault().invokeWhenUIReady(() -> {
            try {
                String buildInfo = BuildInfo.get();
                LOG.log(Level.INFO, "{0} branding.buildInfo={1}", new Object[]{new Throwable().getStackTrace()[0].toString(), buildInfo});
            } catch (IOException ex) {
                LOG.log(Level.INFO, ex.getMessage());
                Arrays.asList(ex.getStackTrace()).stream().forEach((ste) -> LOG.log(Level.INFO, ste.toString()));
            }
            WindowManager.getDefault().getMainWindow().setTitle("Astrolabe Desktop");
        });
    }

}
