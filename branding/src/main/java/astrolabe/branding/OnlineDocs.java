/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 */
package astrolabe.branding;

import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import org.openide.awt.ActionID;
import org.openide.awt.ActionReference;
import org.openide.awt.ActionRegistration;
import org.openide.util.Exceptions;
import org.openide.util.NbBundle.Messages;

/**
 * An action class to add an online docs link to the UI.
 */
@ActionID(
        category = "Help",
        id = "astrolabe.branding.OnlineDocs"
)
@ActionRegistration(
        iconBase = "astrolabe/branding/astrolabe-logo-16.png",
        displayName = "#CTL_OnlineDocs"
)
@ActionReference(path = "Menu/Help", position = 1450)
@Messages("CTL_OnlineDocs=Online Docs")
public final class OnlineDocs implements ActionListener {

    private static final String ONLINE_DOCS_LINK = "https://gitlab.com/arizona-astrolabe/desktop/astrolabe-desktop/-/wikis/home";

    /**
     * Default constructor.
     */
    public OnlineDocs() {
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            Desktop.getDesktop().browse(new URI(ONLINE_DOCS_LINK));
        } catch (URISyntaxException | IOException ex) {
            Exceptions.printStackTrace(ex);
        }
    }
}
