/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */
package astrolabe.common.ui;

import java.awt.Component;
import java.awt.Rectangle;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

/**
 * A class that holds a single, static method for fixing the width of columns in a JTable.
 *
 * @author agardner
 */
public class ColumnFixer {

    public static void resizeColumns(JTable table) {
        TableCellRenderer cellRenderer = new DefaultTableCellRenderer();

        for (int column = 0; column < table.getColumnCount(); column++) {
            TableColumn tableColumn = table.getColumnModel().getColumn(column);
            int maxWidth = tableColumn.getMaxWidth();
            int intercell = table.getIntercellSpacing().width;

            Rectangle r = table.getTableHeader().getHeaderRect(column);
            int preferredWidth = r.width + intercell;
            for (int row = 0; row < table.getRowCount(); row++) {
                Component c = table.prepareRenderer(cellRenderer, row, column);
                int width = c.getPreferredSize().width + intercell;
                preferredWidth = Math.max(preferredWidth, width);

                //  We've exceeded the maximum width, no need to check others.
                if (preferredWidth >= maxWidth) {
                    preferredWidth = maxWidth;
                    break;
                }
            }

            tableColumn.setPreferredWidth(preferredWidth);
        }
    }
}
