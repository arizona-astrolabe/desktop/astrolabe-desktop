/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */
package astrolabe.common.ui;

import java.awt.Component;
import java.awt.Font;
import java.awt.FontMetrics;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * Adjust height of cells in a JTable based on font.
 *
 * @author agardner
 */
public class AutoHeightTableCellRenderer extends DefaultTableCellRenderer {

    /**
     * Set this cell rendered as the renderer on a column in a table.
     *
     * @param table The table to set the cell renderer on.
     * @param colNum The column within the table to set the renderer on.
     */
    public static void set(JTable table, int colNum) {
        table.getColumnModel().getColumn(colNum).setCellRenderer(new AutoHeightTableCellRenderer());
    }

    /**
     * Overridden method to fix a table row's height given the size of the font.
     *
     * @param table
     * @param value
     * @param isSelected
     * @param hasFocus
     * @param row
     * @param column
     * @return
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        Font f = c.getFont();
        FontMetrics fm = c.getFontMetrics(f);
        table.setRowHeight(row, fm.getHeight());
        return c;
    }
}
