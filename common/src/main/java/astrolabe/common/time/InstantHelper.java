/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */
package astrolabe.common.time;

import java.time.Instant;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Utility methods for java.time.Instant.
 *
 * @author agardner
 */
public class InstantHelper {

    // Get the ISO 8061 week of year. 
    public static int getWeekOfYear(Instant inst) {
        var c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        c.setTimeInMillis(inst.toEpochMilli());
        c.setMinimalDaysInFirstWeek(4);
        c.setFirstDayOfWeek(Calendar.MONDAY);
        return c.get(Calendar.WEEK_OF_YEAR);
    }

    // Get the ISO 8061 week-year. 
    public static int getWeekYear(Instant inst) {
        var c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        c.setTimeInMillis(inst.toEpochMilli());
        c.setMinimalDaysInFirstWeek(4);
        c.setFirstDayOfWeek(Calendar.MONDAY);
        return c.getWeekYear();
    }

    // Get the ISO 8061 week of year. 
    public static String getWeekAndYear(Instant inst) {
        var c = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
        c.setTimeInMillis(inst.toEpochMilli());
        c.setMinimalDaysInFirstWeek(4);
        c.setFirstDayOfWeek(Calendar.MONDAY);
        var yr = c.getWeekYear();
        var wk = c.get(Calendar.WEEK_OF_YEAR);
        return String.format("%04d-%02d", yr, wk);
    }
}
