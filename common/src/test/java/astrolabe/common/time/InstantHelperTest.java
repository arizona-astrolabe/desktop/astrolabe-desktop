/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University 
 * of Arizona. All rights reserved.
 * 
 * This file is part of Astrolabe. Astrolabe is free software: you can 
 * redistribute it and/or modify it under the terms of the GNU General Public 
 * License as  published by the Free Software Foundation, either version 3 of 
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See 
 * the GNU General Public License for more details. You should have received a 
 * copy of the GNU General Public License along with Astrolabe. 
 * If not, see <https://www.gnu.org/licenses/>. 
 *
 */
package astrolabe.common.time;

import java.time.Instant;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

/**
 *
 * @author agardner
 */
public class InstantHelperTest {

    public InstantHelperTest() {
    }

    @Test
    public void test1() {
        assertEquals(35, InstantHelper.getWeekOfYear(Instant.parse("2024-08-28T12:00:00Z")));
        assertEquals(31, InstantHelper.getWeekOfYear(Instant.parse("2025-08-01T12:00:00Z")));
        assertEquals(2, InstantHelper.getWeekOfYear(Instant.parse("2025-01-06T12:00:00Z")));
    }

    @Test
    public void test2() {
        assertEquals("2024-35", InstantHelper.getWeekAndYear(Instant.parse("2024-08-28T12:00:00Z")));
        assertEquals("2025-31", InstantHelper.getWeekAndYear(Instant.parse("2025-08-01T12:00:00Z")));
        assertEquals("2025-02", InstantHelper.getWeekAndYear(Instant.parse("2025-01-06T12:00:00Z")));

        // The first week of 2025 includes the last two days of 2024 (since an ISO 8061 week has to include at the least 4 days).
        assertEquals("2025-01", InstantHelper.getWeekAndYear(Instant.parse("2024-12-30T12:00:00Z")));
    }

    @Test
    public void test3() {
        assertEquals(2025, InstantHelper.getWeekYear(Instant.parse("2024-12-30T12:00:00Z")));
    }
}
