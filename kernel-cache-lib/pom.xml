<?xml version="1.0" encoding="UTF-8"?>

<!--
 - Copyright (c) 2024 The Arizona Board of Regents on behalf of the University
 - of Arizona. All rights reserved.
 -
 - This file is part of Astrolabe. Astrolabe is free software: you can
 - redistribute it and/or modify it under the terms of the GNU General Public
 - License as  published by the Free Software Foundation, either version 3 of
 - the License, or  (at your option) any later version. Astrolabe is distributed
 - in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 - implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 - the GNU General Public License for more details. You should have received a
 - copy of the GNU General Public License along with Astrolabe.
 - If not, see <https://www.gnu.org/licenses/>.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" 
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    
    <modelVersion>4.0.0</modelVersion>
    
    <parent>
        <groupId>edu.arizona.space</groupId>
        <artifactId>astrolabe-desktop</artifactId>
        <version>23.0</version>
    </parent>
    
    <groupId>edu.arizona.space</groupId>
    <artifactId>kernel-cache-lib</artifactId>
    <version>1.1</version>
    <packaging>jar</packaging>
    
    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <maven.compiler.release>21</maven.compiler.release>
    </properties>
    
    <licenses>
        <license>
            <name>GNU General Public License, Version 3</name>
            <url>https://www.gnu.org/licenses/gpl-3.0.txt</url>
            <distribution>repo</distribution>
            <comments>Copyright (c) 2024 The Arizona Board of Regents on behalf of The University of Arizona.</comments>
        </license>
    </licenses>
    
    <dependencies>
        <!-- Unit test dependencies. -->
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>5.11.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-params</artifactId>
            <version>5.11.1</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.11.1</version>
            <scope>test</scope>
        </dependency>
        
        <!-- Persistence dependencies. --> 
        <dependency>
            <groupId>jakarta.persistence</groupId>
            <artifactId>jakarta.persistence-api</artifactId>
            <version>3.2.0</version>
        </dependency>
        <dependency>
            <groupId>org.eclipse.persistence</groupId>
            <artifactId>org.eclipse.persistence.jpa</artifactId>
            <version>4.0.4</version>
        </dependency>
        <dependency>
            <groupId>org.ow2.asm</groupId>
            <artifactId>asm</artifactId>
            <version>9.7</version>  
        </dependency>
        <dependency>
            <groupId>org.ow2.asm</groupId>
            <artifactId>asm-commons</artifactId>
            <version>9.7</version> 
        </dependency>
        
        <!-- Source dependencies. -->
        <dependency>
            <groupId>com.h2database</groupId>
            <artifactId>h2</artifactId>
            <version>2.3.232</version>
        </dependency>
        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.18.34</version>
        </dependency>
        <dependency>
            <groupId>com.squareup.okhttp3</groupId>
            <artifactId>okhttp</artifactId>
            <version>5.0.0-alpha.14</version>
        </dependency>
        <dependency>
            <groupId>edu.arizona.space</groupId>
            <artifactId>jnaspice</artifactId>
            <version>67.2</version>
        </dependency>
    </dependencies>
    
    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
    
    <repositories>
        <repository>
            <id>astrolabe-maven-public</id>
            <url>https://gitlab.com/api/v4/projects/58890905/packages/maven</url>
        </repository>
    </repositories>
    <distributionManagement>
        <repository>
            <id>astrolabe-maven-public</id>
            <url>https://gitlab.com/api/v4/projects/58890905/packages/maven</url>
        </repository>
    </distributionManagement>
    
</project>