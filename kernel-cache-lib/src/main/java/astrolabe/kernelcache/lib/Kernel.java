/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University
 * of Arizona. All rights reserved.
 *
 * This file is part of Astrolabe. Astrolabe is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as  published by the Free Software Foundation, either version 3 of
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with Astrolabe.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package astrolabe.kernelcache.lib;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;
import lombok.Getter;
import lombok.Setter;

/**
 * An entity class to represent a kernel in the cache.
 *
 * @author asg
 */
@Entity
@Table(name = "KERNELS")
@NamedQueries({
    @NamedQuery(name = "Kernel.getAll", query = "SELECT x FROM Kernel x"),
    @NamedQuery(name = "Kernel.getByRemotePath", query = "SELECT x FROM Kernel x WHERE x.kernelPK.remotePath = :remotePath")
})
public class Kernel implements Serializable {

    @Getter
    @EmbeddedId
    protected KernelPK kernelPK;

    @Getter
    @Setter
    @Basic(optional = false)
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "LAST_DOWNLOADED")
    protected Date lastDownloaded;

    public Kernel() {
    }

    public Kernel(String prefix, String remotePath, String localPath) {
        kernelPK = new KernelPK(prefix, remotePath, localPath);
        lastDownloaded = new Date();
    }
}
