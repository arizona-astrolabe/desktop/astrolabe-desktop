/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University
 * of Arizona. All rights reserved.
 *
 * This file is part of Astrolabe. Astrolabe is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as  published by the Free Software Foundation, either version 3 of
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with Astrolabe.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package astrolabe.kernelcache.lib;

import jakarta.persistence.Basic;
import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import java.io.Serializable;
import lombok.Getter;
import lombok.Setter;

/**
 * An entity class to represent the primary key of a kernel in the cache.
 *
 * @author asg
 */
@Embeddable
public class KernelPK implements Serializable {

    @Getter
    @Setter
    @Basic(optional = false)
    @Column(name = "PREFIX")
    private String prefix; // e.g., "NAIF"

    @Getter
    @Setter
    @Basic(optional = false)
    @Column(name = "REMOTE_PATH")
    private String remotePath; // e.g. "lsk/naif0012.tls"

    @Getter
    @Setter
    @Basic(optional = false)
    @Column(name = "LOCAL_PATH")
    private String localPath; // e.g., /home/asg/.astrolabe-data/kernel-cache/NAIF/lsk/naif0012.tls

    public KernelPK() {
    }

    public KernelPK(String prefix, String remotePath, String localPath) {
        this.prefix = prefix;
        this.remotePath = remotePath;
        this.localPath = localPath;
    }
}
