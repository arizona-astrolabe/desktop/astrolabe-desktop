/*
 * Copyright (c) 2024 The Arizona Board of Regents on behalf of the University
 * of Arizona. All rights reserved.
 *
 * This file is part of Astrolabe. Astrolabe is free software: you can
 * redistribute it and/or modify it under the terms of the GNU General Public
 * License as  published by the Free Software Foundation, either version 3 of
 * the License, or  (at your option) any later version. Astrolabe is distributed
 * in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
 * implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See
 * the GNU General Public License for more details. You should have received a
 * copy of the GNU General Public License along with Astrolabe.
 * If not, see <https://www.gnu.org/licenses/>.
 */
package astrolabe.kernelcache.lib;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.Persistence;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Cache for SPICE kernels.
 *
 * @author asg
 */
public class KernelCache {

    private static final Logger LOG = Logger.getLogger(KernelCache.class.getName());

    static private boolean initialized = false;
    static private EntityManagerFactory emf;
    static private EntityManager em;
    static private HashMap<String, String> prefixToUrl;

    private static void init() throws IOException {
        if (initialized) {
            return;
        } else {
            initialized = true;
        }

        emf = Persistence.createEntityManagerFactory("edu.arizona.space_astrolabe-kernel-cache_nbm_1.0PU");
        em = emf.createEntityManager();

        // Make sure there's a directory called ~/.astrolabe-data.
        LOG.log(Level.INFO, "Checking for {0}/.astrolabe-data", System.getProperty("user.home"));
        Path dataDir = Paths.get(System.getProperty("user.home") + "/.astrolabe-data");
        if (!dataDir.toFile().exists()) {
            dataDir.toFile().mkdirs();
        }

        // Try and get the kernel cache configuration file.
        LOG.log(Level.INFO, "Checking for {0}", dataDir.resolve("kernel-cache.csv").toFile().getCanonicalPath());
        var cfg = dataDir.resolve("kernel-cache.csv");
        if (!cfg.toFile().exists()) {
            // Kernel cache does not yet exist. Create it and populate it with the NAIF generics kernel archive.
            try (var fos = new FileOutputStream(cfg.toFile())) {
                fos.write(String.format("%s,%s\n", "NAIF", "https://naif.jpl.nasa.gov/pub/naif/generic_kernels/").getBytes());
            } catch (IOException ex) {
                LOG.log(Level.SEVERE, ex.toString());
            }
        }
        // Read in the configuration data.
        prefixToUrl = new HashMap<>();
        try (var scanner = new Scanner(cfg.toFile());) {
            scanner.useDelimiter(",");
            while (scanner.hasNext()) {
                String prefix = scanner.next().trim(), url = scanner.next().trim();
                prefixToUrl.put(prefix, url);
            }
        } catch (FileNotFoundException ex) {
            LOG.log(Level.SEVERE, ex.toString());
        }
    }

    /**
     * Ascertain whether a kernel is in the cache.
     *
     * @param prefix
     * @param remotePath
     * @return
     * @throws java.io.IOException
     */
    public static boolean contains(String prefix, String remotePath) throws IOException {
        init();

        // Check to if it's in the cache first.
        List list = em.createNamedQuery("Kernel.getByRemotePath").setParameter("remotePath", remotePath).getResultList();
        return !list.isEmpty();
    }

    /**
     * Get the local path of a kernel in the cache. For example, get("NAIF", "lsk/naif0012.tls");
     *
     * @param prefix e.g., NAIF.
     * @param remotePath e.g., lsk/naif0012.tls
     * @return The local path to the kernel in the cache.
     * @throws java.io.IOException
     * @throws java.net.URISyntaxException
     */
    public static Path get(String prefix, String remotePath) throws IllegalArgumentException, IOException, URISyntaxException {
        init();

        // Check to if it's in the cache first.
        List list = em.createNamedQuery("Kernel.getByRemotePath").setParameter("remotePath", remotePath).getResultList();
        if (!list.isEmpty() && list.get(0) instanceof Kernel k) {
            // It's in the cache.
            return Paths.get(k.getKernelPK().getLocalPath());
        } else {
            // It's not in the cache.
            var baseUrl = prefixToUrl.get(prefix);
            if (baseUrl == null) {
                throw new IllegalArgumentException(String.format("The prefix '%s' is unknown to the kernel cache.", prefix));
            } else {
                // We have a prefix, a remote path, and a base URL. Make a local path for the kernel
                // and try and download it.
                var outputPath = Paths.get(System.getProperty("user.home"), "/.astrolabe-data/kernel-cache/", prefix, remotePath);
                var outputParent = outputPath.getParent();
                if (!outputParent.toFile().exists()) {
                    outputParent.toFile().mkdirs();
                }
                var client = new OkHttpClient();
                var request = new Request.Builder().url(baseUrl + "/" + remotePath).build();
                var response = client.newCall(request).execute();
                var body = response.body();
                if (body != null) {
                    InputStream in = body.byteStream();
                    try (FileOutputStream out = new FileOutputStream(outputPath.toFile())) {
                        byte[] buf = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = in.read(buf)) != -1) {
                            out.write(buf, 0, bytesRead);
                        }
                    }

                    // Persist to the database so we can find it later.
                    var kernel = new Kernel(prefix, remotePath, outputPath.toFile().getCanonicalPath());
                    em.getTransaction().begin();
                    em.persist(kernel);
                    em.getTransaction().commit();
                    return outputPath;
                } else {
                    throw new IllegalArgumentException("Could not find kernel in remote.");
                }
            }
        }
    }
}
